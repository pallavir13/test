public class inlineAccountRevenue {
       
    private ApexPages.StandardController controller {get; set;}
    private Account a;
    public Id accountIdPage{get;set;}
    private ID accountId;
    public List<POC_Revenue__c> revenueList  {get;set;}
    public POC_Revenue__c prevenadata {get;set;}
    public list<WrapperclassNew> wrap {get;set;}
    public list<WrapperclassNew> wrapYTDUnits {get;set;}
    public string selectedTab {get;set;}
    public string UltaRental2016,UltaRental2017,VerafloRev2016,VerafloRev2017,UltaPlacements2016,UltaPlacements2017,VerafloOppty2016,VerafloOppty2017;
    public string VerafloPlacements2016,VerafloPlacements2017,VerafloPenetration2016,VerafloPenetration2017;
    // Home Grown
    public List<POC_Revenue__c> revenueListHomeGrown  {get;set;}
    
    
    
    public class WrapperclassNew{
        public string str1{get;set;}
        public string str3{get;set;}
        public string str2{get;set;}
        public WrapperclassNew(String s1, string s2, string s3){
            this.str1 = s1;
            this.str2 = s2;
            this.str3 = s3;
        }
    }
    
    public class WrapperclassHomeGrown{
        public string str1{get;set;}
        public string str3{get;set;}
        public string str2{get;set;}
        public WrapperclassHomeGrown(String s1, string s2, string s3){
            this.str1 = s1;
            this.str2 = s2;
            this.str3 = s3;
        }
    }
   
    
    public inlineAccountRevenue(ApexPages.StandardController controller) {
        wrap = new List<wrapperclassNew>();
        wrapYTDUnits = new List<wrapperclassNew>();
        this.controller = controller;
        this.a = (Account)controller.getRecord();
        accountId = controller.getId();
        accountIdPage = controller.getId();
        selectedTab = 'name1';
        
        revenueList = [SELECT AbThera_YoY__c,Account_Name__c,Area__c,Category__c,CreatedById,
                       CreatedDate,Cust__c,Dist__c,Field_DCA__c,GPO__c,Id,IDN__c,IsDeleted,
                       MMC_Flag__c,Monthly_Revenue__c,Month__c,Name,Name__c,OwnerId,Prevena_YoY__c,
                       Product_Group__c,Region__c,Rental_Revenue_YoY__c,Report_Date__c,Rep__c,ST__c,
                       Territory__c,Total_Rev_YoY__c,Ulta_Placements_468__c,
                       Ulta_Rental_YTD__c,VAC_Disp_Via_YoY__c,Veraflo_Oppty_40__c,Veraflo_Penetration__c,
                       Veraflo_Placements_350__c,Veraflo_Rev_YTD__c,Veraflo_YoY__c,Year__c,
                       YOY_Penetration__c,Graft_Jacket_YoY__c,Orders_YoY__c,UIU_YoY__c,
                       Trans_YoY__c,Core_Revenue_YoY__c                       
                       FROM POC_Revenue__c 
                       WHERE Category__c = 'page' AND Account_Name__c =:accountId];
        
       
        for(POC_Revenue__c pr :revenueList )
        {
            if(pr.Product_Group__c == 'Prevena')
            {
                prevenadata = pr;
            }
           if(pr.Year__c == 2016)
            {
                UltaRental2016 ='USD '+ String.valueOf(pr.Ulta_Rental_YTD__c);
                VerafloRev2016 ='USD '+ String.valueOf(pr.Veraflo_Rev_YTD__c);
                UltaPlacements2016 = String.valueOf(pr.Ulta_Placements_468__c);
                VerafloOppty2016 = String.valueOf(pr.Veraflo_Oppty_40__c);    
                VerafloPlacements2016 = String.valueOf(pr.Veraflo_Placements_350__c);
                VerafloPenetration2016 = String.valueOf(pr.Veraflo_Penetration__c)+'%';
            }
            else if(pr.Year__c == 2017)
            {
                UltaRental2017 ='USD '+ String.valueOf(pr.Ulta_Rental_YTD__c);
                VerafloRev2017 ='USD '+ String.valueOf(pr.Veraflo_Rev_YTD__c);
                UltaPlacements2017 = String.valueOf(pr.Ulta_Placements_468__c);
                VerafloOppty2017 = String.valueOf(pr.Veraflo_Oppty_40__c);
                VerafloPlacements2017 = String.valueOf(pr.Veraflo_Placements_350__c);
                VerafloPenetration2017 = String.valueOf(pr.Veraflo_Penetration__c)+'%';
            }
            
        }
        	wrap.add(new WrapperclassNew('Ulta Rental YTD', UltaRental2016, UltaRental2017)) ;
            wrap.add(new WrapperclassNew('Veraflo Rev YTD', VerafloRev2016, VerafloRev2017)) ;
            wrapYTDUnits.add(new WrapperclassNew('Ulta Placement @ $468', UltaPlacements2016, UltaPlacements2017)) ;
            wrapYTDUnits.add(new WrapperclassNew('Veraflo Oppty 40%', VerafloOppty2016, VerafloOppty2017)) ;
        	wrapYTDUnits.add(new WrapperclassNew('Veraflo Placements @ $350', VerafloPlacements2016, VerafloPlacements2017)) ;
        	wrapYTDUnits.add(new WrapperclassNew('Veraflo Penetration', VerafloPenetration2016, VerafloPenetration2017)) ;
        
                
        
    }
   

}